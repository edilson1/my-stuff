sudo yum install -y -q yum-utils curl jq unzip
TERRAFORM_VER=`curl -s https://api.github.com/repos/hashicorp/terraform/releases/latest |  grep tag_name | cut -d: -f2 | tr -d \"\,\v | awk '{$1=$1};1'`
wget https://releases.hashicorp.com/terraform/${TERRAFORM_VER}/terraform_${TERRAFORM_VER}_linux_amd64.zip
unzip terraform_${TERRAFORM_VER}_linux_amd64.zip
sudo mv terraform /usr/local/bin/
terraform version
touch /tmp/credentials
curl -H "Authorization: $AWS_CONTAINER_AUTHORIZATION_TOKEN" $AWS_CONTAINER_CREDENTIALS_FULL_URI 2>/dev/null > /tmp/credentials
ACCESS_KEY=`cat /tmp/credentials| jq -r .AccessKeyId`
SECRET_KEY=`cat /tmp/credentials| jq -r .SecretAccessKey`
SESSION_TOKEN=`cat /tmp/credentials| jq -r .Token`
ACCOUNTID=$(aws sts get-caller-identity --query "Account" --output text)
clear
echo -ne "\n##### This is a temporary credential for the accountID $ACCOUNTID. ##### \n\n aws_access_key_id=${ACCESS_KEY}\n aws_secret_access_key=${SECRET_KEY}\n aws_session_token=${SESSION_TOKEN}\n\n" | awk '{$1=$1};1' | tee /tmp/creds

